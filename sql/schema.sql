create table users (
	id int not null
,	email varchar(255) character set utf8
,	displayname varchar(255) character set utf8
,	password varchar(128) character set utf8
,	salt varchar(22) character set utf8
,	primary key (id)
);

create table roles (
	id int not null
,	name varchar(255) character set utf8
,	primary key (id)
);

create table permissions (
	id int not null
,	name varchar(255) character set utf8
,	readable boolean
,	writeable boolean
,	updateable boolean
,	deleteable boolean
,	primary key (id)
);

create table tags (
	id int not null
,	name varchar(255) character set utf8
,	primary key (id)
);

create table entries (
	id int not null
,	title varchar(255) character set utf8
,	createdby int
,	creationdate datetime
,	modifiedby int
,	modificationdate datetime
,	primary key (id)
,	foreign key (createdby) references users(id)
,	foreign key (modifiedby) references users(id)
);

create table entry2tagrel (
	id int not null
,	entryid int not null
,	tagid int not null
,	primary key (id)
,	foreign key (entryid) references entries(id)
,	foreign key (tagid) references tags(id)
);

create table user2rolerel (
	id int not null
,	userid int not null
,	roleid int not null
,	primary key (id)
,	foreign key (userid) references users(id)
,	foreign key (roleid) references roles(id)
);

create table role2permissionrel (
	id int not null
,	roleid int not null
,	permissionid int not null
,	primary key (id)
,	foreign key (roleid) references roles(id)
,	foreign key (permissionid) references permissions(id)
);

create table props (
    id varchar(100)
,   value varchar(255)
,   primary key (id)
);

insert into users (
	id
,	email
,	displayname
,	password
,	salt
) values (
	1
,	'admin@server.com'
,	'admin'
,	'b50fb3fd9f88b5aaf9945335d72b9d2c67e017a6378496163da7469d572e50bf20da52078fe02a08d955a677d1751650e56919628b50e3526aa1b9f1d492e777'
,	'3rx2FIflqzWwGM6vitgU9A'
);