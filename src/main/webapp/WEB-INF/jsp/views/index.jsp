<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <title>This is a title!</title>
    </head>
    <body>
        <h2>Hello World!</h2>
        <h4>header 4</h4>
        <p>testing 1-2-3-4</p>
        <p>Another section, just testing out bootstrap!</p>
        <p>testinterceptor: <c:out value="${requestScope.testinterceptor}" /></p>
        <h4>Users:</h4>
        <ul>
            <c:forEach var="user" items="${users}">
                <li><c:out value="${user.displayName}" /> | <c:out value="${user.email}" /></li>
            </c:forEach>
        </ul>
    </body>
</html>
