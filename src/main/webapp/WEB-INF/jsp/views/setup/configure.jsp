<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib tagdir="/WEB-INF/tags/forms" prefix="f" %>

<html>
    <head>
        <title><spring:message code="setup.title" /></title>
    </head>
    <body>

        <form:form method="post" action="${nextStep}" id="configForm" modelattribute="config" commandName="config" class="form-horizontal">
            <fieldset>
                <legend><spring:message code="setup.configure.title" /></legend>
                <form:errors class="text-error"></form:errors>
                <spring:bind path="blogname">
                    <f:admininput name="blogname" status="${status}" root="configure" />
                </spring:bind>
                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="btn btn-large btn-primary"><spring:message code="setup.configure.submit" /></button>
                    </div>
                </div>
            </fieldset>
        </form:form>

    </body>
</html>