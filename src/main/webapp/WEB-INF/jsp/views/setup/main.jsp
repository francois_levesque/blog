<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<html>
    <head>
        <title><spring:message code="setup.title" /></title>
    </head>
    <body>

        <p>
            <a class="btn btn-primary btn-large" href="${nextStep}">
              <spring:message code="setup.getstarted" />
            </a>
        </p>

    </body>
</html>