<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib tagdir="/WEB-INF/tags/forms" prefix="f" %>

<html>
    <head>
        <title><spring:message code="setup.title" /></title>
    </head>
    <body>

        <form:form method="post" action="${nextStep}" id="getStartedForm" modelattribute="adminuser" commandName="adminuser" class="form-horizontal">
            <fieldset>
                <legend><spring:message code="setup.create.admin.title" /></legend>
                <form:errors class="text-error"></form:errors>
                <c:if test="${userExists}">
                    <div class="text-error"><spring:message code="setup.create.admin.error.userexists" /></div>
                </c:if>
                <spring:bind path="username">
                    <f:admininput name="username" status="${status}" />
                </spring:bind>
                <spring:bind path="email">
                    <f:admininput name="email" status="${status}" />
                </spring:bind>
                <spring:bind path="password">
                    <f:admininput name="password" status="${status}" password="true" />
                </spring:bind>
                <spring:bind path="repassword">
                    <f:admininput name="repassword" status="${status}" password="true" />
                </spring:bind>
                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="btn btn-large btn-primary"><spring:message code="setup.create.admin.submit" /></button>
                    </div>
                </div>
            </fieldset>
        </form:form>

    </body>
</html>