<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="page-header">
    <h1>
        <a href="${pageContext.request.contextPath}"><spring:message code="header.blog.name" /></a>
        <small><em><spring:message code="header.blog.description" /></em></small>
    </h1>
</div>