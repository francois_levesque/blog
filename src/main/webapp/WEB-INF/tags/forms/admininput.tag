<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ attribute name="name" required="true" %>
<%@ attribute name="status" required="true" %>
<%@ attribute name="root" required="false" %>
<%@ attribute name="password" required="false" type="java.lang.Boolean" %>

<c:if test="${empty root}">
	<c:set var="root" value="create.admin" />
</c:if>

<c:if test="${password == null}">
	<c:set var="password" value="false" />
</c:if>

<spring:bind path="${name}">
    <div class="control-group ${status.error ? 'error' : ''}">
        <form:label path="${name}" class="control-label"><spring:message code="setup.${root}.${name}" /></form:label>
        <div class="controls">
					<spring:message code="setup.${root}.placeholder.${name}" var="placeholder" />
            <c:choose>
            	<c:when test="${password}">
            		<form:password path="${name}" placeholder="${placeholder}" />
            	</c:when>
            	<c:otherwise>
            		<form:input path="${name}" placeholder="${placeholder}" />
            	</c:otherwise>
            </c:choose>
            <form:errors path="${name}" class="help-inline"></form:errors>
        </div>
    </div>
</spring:bind>