<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><decorator:title /></title>
		<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap.min.css" />
		<script src="<%= request.getContextPath() %>/js/jquery-1.10.2.min.js"></script>
		<script src="<%= request.getContextPath() %>/js/setup.js"></script>
	</head>
	<body>
	    <jsp:include page="/WEB-INF/jsp/includes/header.jsp" />
		<div class="container-fluid">
		    <div class="row-fluid">
                <div class="span2">
                    <jsp:include page="/WEB-INF/jsp/includes/setup_leftnav.jsp" />
                </div>
                <div class="span10">
                    <div class="hero-unit">
                        <h3><spring:message code="setup.welcome" /></h3>
                        <p><spring:message code="setup.follow" /></p>
                    </div>
                        <div>
                            <decorator:body />
                        </div>
                </div>
            </div>
		</div>
	</body>
</html>