<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><decorator:title /></title>
		<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap.min.css" />
	</head>
	<body>
	    <jsp:include page="/WEB-INF/jsp/includes/header.jsp" />
		<div class="container-fluid">
		    <div class="row-fluid">
                <div class="span2">
                    <jsp:include page="/WEB-INF/jsp/includes/leftnav.jsp" />
                </div>
                <div class="span10">
                    <decorator:body />
                </div>
            </div>
		</div>
	</body>
</html>