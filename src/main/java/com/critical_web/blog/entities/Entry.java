package com.critical_web.blog.entities;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

/**
 * User: Francois Levesque
 * Date: 9/11/13
 * Time: 7:19 PM
 */
@Entity
@Table(name="entries")
public class Entry {

    @Id
    private int id;

    @OneToMany
    private Collection<Tag> tags;

    private String title;

    @OneToOne
    private User createdBy;

    private Date creationDate;

    @OneToOne
    private User modifiedBy;

    private Date modificationDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Collection<Tag> getTags() {
        return tags;
    }

    public void setTags(Collection<Tag> tags) {
        this.tags = tags;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public User getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }
}
