package com.critical_web.blog.entities;

import javax.persistence.*;
import java.util.Collection;

/**
 * User: Francois Levesque
 * Date: 9/11/13
 * Time: 7:21 PM
 */
@Entity()
@Table(name="users")
public class User {

    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String email;

    private String displayName;

    private String password;

    private String salt;

    @OneToMany
    private Collection<Role> roles;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }
}
