package com.critical_web.blog.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

/**
 * User: Francois Levesque
 * Date: 9/11/13
 * Time: 7:23 PM
 */
@Entity
@Table(name="roles")
public class Role {

    @Id
    private int id;

    private String name;

    @OneToMany
    private Collection<Permission> permissions;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Collection<Permission> permissions) {
        this.permissions = permissions;
    }
}
