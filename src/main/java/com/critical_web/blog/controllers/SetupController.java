package com.critical_web.blog.controllers;

 /*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
 
 /*
 * Author: flevesque
 *
 */

import com.critical_web.blog.exceptions.user.UserExistsException;
import com.critical_web.blog.form.AdminUser;
import com.critical_web.blog.form.Config;
import com.critical_web.blog.services.UserService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;

@Controller
@RequestMapping("/setup")
public class SetupController {

	private static final String STEP_GET_STARTED = "/setup/get-started";
	private static final String STEP_CREATE_ADMIN_USER = "/setup/create-admin-user";
	private static final String STEP_CONFIGURE = "/setup/configure";
	private static final String STEP_STORE_CONFIG = "/setup/store-config";
	private static final String STEP_REVIEW_CONFIG = "/setup/review";

	private static final Logger LOG = Logger.getLogger(SetupController.class);

	@Resource
	UserService userService;

	@RequestMapping(value = {"/", ""})
	public ModelAndView main(HttpServletRequest request) {
		final ModelAndView modelAndView = new ModelAndView("setup/main");
		modelAndView.addObject("nextStep", request.getContextPath() + STEP_GET_STARTED);
		return modelAndView;
	}

	@RequestMapping("/get-started")
	public ModelAndView getStarted(HttpServletRequest request) {
		final ModelAndView modelAndView = new ModelAndView("setup/getstarted", "adminuser", new AdminUser());
		modelAndView.addObject("nextStep", request.getContextPath() + STEP_CREATE_ADMIN_USER);
		return modelAndView;
	}

	@RequestMapping(value = "/create-admin-user", method = RequestMethod.POST)
	public ModelAndView createAdminUser(@Valid @ModelAttribute("adminuser") AdminUser adminUser,
										BindingResult bindingResult,
										HttpServletRequest request, ModelAndView modelAndView,
										HttpServletResponse response) throws IOException {

		modelAndView.setViewName("/setup/getstarted");
		modelAndView.addObject("adminuser", adminUser);

		if (bindingResult.hasErrors()) {

			for (ObjectError e : bindingResult.getAllErrors()) {
				LOG.error("error: " + e);
			}

			return modelAndView;
		}

		try {
			userService.createNewUser(adminUser.getUsername(), adminUser.getEmail(), adminUser.getPassword());
		} catch (UserExistsException e) {
			LOG.error("user exists");
			modelAndView.addObject("userExists", true);
			return modelAndView;
		}

		response.sendRedirect(request.getContextPath() + STEP_CONFIGURE);

		return modelAndView;
	}

	@RequestMapping("/configure")
	public ModelAndView configure(HttpServletRequest request) {
		final ModelAndView modelAndView = new ModelAndView("setup/configure", "config", new Config());
		modelAndView.addObject("nextStep", request.getContextPath() + STEP_STORE_CONFIG);
		return modelAndView;
	}

	@RequestMapping("/store-config")
	public ModelAndView storeConfig(@Valid @ModelAttribute("config") Config config, BindingResult bindingResult,
									HttpServletRequest request, ModelAndView modelAndView,
									HttpServletResponse response) throws IOException {

		modelAndView.setViewName("/setup/configure");
		modelAndView.addObject("config", config);

		if (bindingResult.hasErrors()) {

			for (ObjectError e : bindingResult.getAllErrors()) {
				LOG.error("error: " + e);
			}

			return modelAndView;
		}

		return null;
	}

	@RequestMapping("/admin-exists")
	public ModelAndView adminUserExists(HttpServletRequest request) {
		return new ModelAndView("setup/adminuserexists");
	}


}
