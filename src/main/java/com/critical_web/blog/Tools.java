package com.critical_web.blog;

import org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * User: Francois Levesque
 * Date: 9/11/13
 * Time: 10:17 PM
 */
public class Tools {

    public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        int size = 16;
        byte[] bytes = new byte[size];
        new Random().nextBytes(bytes);
        String output = Base64.encodeBase64URLSafeString(bytes);
        System.out.println("salt: " + output + " (" + output.length() + ")");

        MessageDigest md = MessageDigest.getInstance("SHA-512");
        byte[] digest = md.digest((output + "admin").getBytes("UTF-8"));
        String hexed = String.format("%0128x", new BigInteger(1, digest));
        System.out.println("password: " + hexed + " (" + hexed.length() + ")");

        digest = md.digest((output + "admin").getBytes("UTF-8"));
        hexed = String.format("%0128x", new BigInteger(1, digest));
        System.out.println("password: " + hexed + " (" + hexed.length() + ")");
    }
}
