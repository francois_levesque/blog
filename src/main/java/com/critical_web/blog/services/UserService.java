package com.critical_web.blog.services;

import com.critical_web.blog.daos.hibernate.HibernateUserDao;
import com.critical_web.blog.exceptions.AmbiguousIdentifierException;
import com.critical_web.blog.exceptions.user.UserExistsException;

/**
 * User: Francois Levesque
 * Date: 9/24/13
 * Time: 1:19 PM
 */
public interface UserService {

    public boolean createNewUser(String username, String email, String password) throws UserExistsException;

    public void setUserDao(HibernateUserDao dao);

    public void setSecurityService(SecurityService securityService);

}
