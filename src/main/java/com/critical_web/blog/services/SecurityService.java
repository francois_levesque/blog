package com.critical_web.blog.services;

/**
 * Created with IntelliJ IDEA.
 * User: rezz
 * Date: 25/12/13
 * Time: 12:41 PM
 * To change this template use File | Settings | File Templates.
 */
public interface SecurityService {

    public String encryptPassword(String password, String salt);

    public String encryptPassword(String password);

    public String getSalt();

}
