package com.critical_web.blog.services.impl;

import com.critical_web.blog.daos.hibernate.HibernateUserDao;
import com.critical_web.blog.entities.User;
import com.critical_web.blog.exceptions.AmbiguousIdentifierException;
import com.critical_web.blog.exceptions.UnknownIdentifierException;
import com.critical_web.blog.exceptions.user.UserExistsException;
import com.critical_web.blog.services.SecurityService;
import com.critical_web.blog.services.UserService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * User: Francois Levesque
 * Date: 9/24/13
 * Time: 2:50 PM
 */
public class DefaultUserService implements UserService {

    public enum Errors {
        ALREADY_EXISTS
    }

    @Resource
    HibernateUserDao userDao;

    @Resource
    SecurityService securityService;

    @Override
    @Transactional
    public boolean createNewUser(String username, String email, String password) throws UserExistsException {

        User user;

        try {
            user = userDao.getUserByDisplayName(username);

            if (user != null) {
                throw new UserExistsException("this user already exists");
            }

        } catch (UnknownIdentifierException e) {
            // nothing, this is expected
        } catch (AmbiguousIdentifierException e) {
            throw new UserExistsException("this user already exists (multiple times");
        }

        user = new User();

        user.setDisplayName(username);
        user.setEmail(email);

        String salt = securityService.getSalt();

        user.setSalt(salt);
        user.setPassword(securityService.encryptPassword(password, salt));

        userDao.save(user);

        return true;
    }

    public void setUserDao(HibernateUserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }
}
