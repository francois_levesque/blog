package com.critical_web.blog.services.impl;

 /*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
 
 /*
 * Author: flevesque
 *
 */

import com.critical_web.blog.daos.hibernate.HibernatePropertyDao;
import com.critical_web.blog.entities.Property;
import com.critical_web.blog.services.StatusService;
import org.hibernate.HibernateException;

import javax.annotation.Resource;

public class DefaultStatusService implements StatusService
{

	@Resource
	HibernatePropertyDao propertyDao;

	@Override
	public boolean isSystemInitialized()
	{
		Property prop;

		try {
			prop = propertyDao.getProperty("isInitialized");
		} catch (HibernateException e) {
			return false;
		}

		if (prop == null) {
			return false;
		}

		String result = prop.getValue();

		if (result == null) {
			return false;
		}

		return Boolean.parseBoolean(result);
	}

	public void setPropertyDao(HibernatePropertyDao propertyDao)
	{
		this.propertyDao = propertyDao;
	}
}
