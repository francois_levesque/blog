package com.critical_web.blog.services.impl;

import com.critical_web.blog.services.SecurityService;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: rezz
 * Date: 25/12/13
 * Time: 12:42 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class DefaultSecurityService implements SecurityService {

    private static final Logger LOG = Logger.getLogger(DefaultSecurityService.class);

    public String encryptPassword(String password, String salt) {

        MessageDigest md = null;

        byte[] digest = new byte[0];

        try {
            md = MessageDigest.getInstance("SHA-512");
            digest = md.digest((salt + password).getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            LOG.warn("error encrypting password.");
        } catch (UnsupportedEncodingException e) {
            LOG.warn("error encrypting password.");
        }

        return String.format("%0128x", new BigInteger(1, digest));

    }

    @Override
    public String encryptPassword(String password) {
        return encryptPassword(password, getSalt());
    }

    @Override
    public String getSalt() {
        int size = 16;
        byte[] bytes = new byte[size];
        new Random().nextBytes(bytes);
        return Base64.encodeBase64URLSafeString(bytes);
    }

}
