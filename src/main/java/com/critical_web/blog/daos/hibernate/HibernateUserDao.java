package com.critical_web.blog.daos.hibernate;

 /*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
 
 /*
 * Author: flevesque
 *
 */

import com.critical_web.blog.entities.User;
import com.critical_web.blog.exceptions.AmbiguousIdentifierException;
import com.critical_web.blog.exceptions.UnknownIdentifierException;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Collection;

@Repository
public class HibernateUserDao
{

	@Resource
	SessionFactory sessionFactory;

	private static final Logger LOG = Logger.getLogger(HibernateUserDao.class);

	/**
	 * Returns a Collection representing all users in the system
	 *
	 * @return a Collection of {@link User} entities
	 */
	public Collection<User> getUsers()
	{
		return getUsers(0, null, null);
	}

	/**
	 * Returns users that match the provided e-mail
	 *
	 * @param email the user's e-mail
	 * @return a Collection of {@link User} entities
	 */
	public Collection<User> getUsers(final String email)
	{
		return getUsers(0, email, null);
	}

	/**
	 * Returns users that match the id
	 *
	 * @param id the user's id
	 * @return a Collection of {@link User} entities
	 */
	public Collection<User> getUsers(final int id)
	{
		return getUsers(id, null, null);
	}

	/**
	 * Returns a user based on his display name.
	 *
	 * @param displayName a String representing the user's display name
	 * @return a {@link User} entity
	 * @throws AmbiguousIdentifierException thrown when more than one record is found
	 * @throws UnknownIdentifierException   thrown when no records are found
	 */
	public User getUserByDisplayName(final String displayName) throws AmbiguousIdentifierException,
			UnknownIdentifierException
	{
		Collection<User> users = getUsers(0, null, displayName);
		if (users.size() == 1) {
			return users.iterator().next();
		} else if (users.size() > 1) {
			throw new AmbiguousIdentifierException("duplicate display name found.");
		} else {
			throw new UnknownIdentifierException("display name not found.");
		}
	}

	/**
	 * Returns users based on the values provided
	 *
	 * @param id          a {@link User} id. Ignored if set to 0.
	 * @param email       String - the user's e-mail. Ignored if null.
	 * @param displayName String - the user's display name. Ignored if null.
	 * @return a Collection of {@link User} entities matching the provided values.
	 */
	public Collection<User> getUsers(final int id, final String email, final String displayName)
	{
		final Session session = sessionFactory.getCurrentSession();

		Criteria criteria = session.createCriteria(User.class);

		if (id != 0) {
			criteria.add(Restrictions.eq("id", id));
		}

		if (email != null) {
			criteria.add(Restrictions.eq("email", email));
		}

		if (displayName != null) {
			criteria.add(Restrictions.eq("displayName", displayName));
		}

		Collection<User> result = criteria.list();

		return result;
	}

	/**
	 * Persists the {@link User}
	 *
	 * @param user the {@link User} to persist
	 */
	public void save(User user)
	{
		final Session session = sessionFactory.getCurrentSession();
		session.save(user);
	}

}
