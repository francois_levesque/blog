package com.critical_web.blog.daos.hibernate;

 /*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
 
 /*
 * Author: flevesque
 *
 */

import com.critical_web.blog.entities.Property;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository
public class HibernatePropertyDao
{

	@Resource
	SessionFactory sessionFactory;

	public Property getProperty(final String name)
	{
		return (Property) sessionFactory.getCurrentSession().get(Property.class, name);
	}

}
