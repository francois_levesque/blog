package com.critical_web.blog.interceptors;

 /*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
 
 /*
 * Author: flevesque
 *
 */

import com.critical_web.blog.services.StatusService;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class InitializedInterceptor extends HandlerInterceptorAdapter
{

	@Resource
	StatusService statusService;

	private static final Logger LOG = Logger.getLogger(InitializedInterceptor.class);

	private static final String REDIRECT_URL = "/setup";

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException
	{

		final String pathInfo = request.getRequestURI().substring(request.getContextPath().length());

		if (LOG.isInfoEnabled()) {
			LOG.info("requestURI: " + request.getRequestURI());
			LOG.info("System not initialized! Redirecting to " + REDIRECT_URL + ".");
		}

		if (!pathInfo.contains(REDIRECT_URL.toLowerCase()) && ! statusService.isSystemInitialized()) {
			response.sendRedirect(request.getContextPath() + REDIRECT_URL);
			return false;
		}

		return true;
	}

}
