package com.critical_web.blog.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * Created by flevesque on 1/2/14.
 */
public class Config {

    @NotNull @NotEmpty
    private String blogname;

    public String getBlogname() {
        return blogname;
    }

    public void setBlogname(String blogname) {
        this.blogname = blogname;
    }

}
