package com.critical_web.blog.form;

 /*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
 
 /*
 * Author: flevesque
 *
 */

import com.critical_web.blog.form.validation.MustMatch;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@MustMatch(first = "password", second="repassword", message = "{MustMatch.adminuser.password}")
public class AdminUser
{

	@NotNull @NotEmpty
	private String username;

	@NotNull @NotEmpty
	private String password;

	@NotNull @NotEmpty
	private String repassword;

	@NotNull @NotEmpty @Email
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getRepassword()
	{
		return repassword;
	}

	public void setRepassword(String repassword)
	{
		this.repassword = repassword;
	}

}
