package com.critical_web.blog.exceptions;

/**
 * User: Francois Levesque
 * Date: 9/24/13
 * Time: 2:59 PM
 */
public class UnknownIdentifierException extends Exception {

    public UnknownIdentifierException(final String message) {
        super(message);
    }

}
