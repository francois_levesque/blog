package com.critical_web.blog.exceptions.user;

/**
 * User: Francois Levesque
 * Date: 11/06/13
 * Time: 16:34 PM
 */
public class UserExistsException extends Exception
{

	public UserExistsException(final String message) {
		super(message);
	}

}
