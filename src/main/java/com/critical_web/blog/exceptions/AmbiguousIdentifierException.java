package com.critical_web.blog.exceptions;

/**
 * User: Francois Levesque
 * Date: 9/24/13
 * Time: 2:56 PM
 */
public class AmbiguousIdentifierException extends Exception {

    public AmbiguousIdentifierException(final String message) {
        super(message);
    }

}
