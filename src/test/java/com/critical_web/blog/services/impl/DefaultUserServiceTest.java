package com.critical_web.blog.services.impl;

import com.critical_web.blog.daos.hibernate.HibernateUserDao;
import com.critical_web.blog.entities.User;
import com.critical_web.blog.exceptions.AmbiguousIdentifierException;
import com.critical_web.blog.exceptions.UnknownIdentifierException;
import com.critical_web.blog.exceptions.user.UserExistsException;
import com.critical_web.blog.services.SecurityService;
import com.critical_web.blog.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * User: Francois Levesque
 * Date: 9/24/13
 * Time: 3:34 PM
 */
public class DefaultUserServiceTest {

    UserService userService;

    @Mock
    HibernateUserDao userDao;

    @Mock
    SecurityService securityService;

    @Before
    public void setUp() {
        initMocks(this);
        userService = new DefaultUserService();
        userService.setUserDao(userDao);
        userService.setSecurityService(securityService);

    }

    @Test
    public void testCreateNewUser() {

        try {
            when(userDao.getUserByDisplayName("test")).thenReturn(null);
        } catch (AmbiguousIdentifierException | UnknownIdentifierException e) {
            fail("Exception thrown - shouldn't happen here!");
        }

        when(securityService.encryptPassword("")).thenReturn("");

		boolean result = false;
		try {
        	result = userService.createNewUser("test", "", "");
		} catch(UserExistsException e) {
			fail("Exception thrown - shouldn't happen here!");
		}

        assertTrue("createNewUser() should return true.", result);

    }

    @Test
    public void testCreateNewUserExists() {

        try {
            User user = new User();
            user.setDisplayName("test");
            when(userDao.getUserByDisplayName("test")).thenReturn(user);
        } catch (AmbiguousIdentifierException | UnknownIdentifierException e) {
            fail("Exception thrown - shouldn't happen here!");
        }

        boolean ambiguous = false;
        try {
            ambiguous = userService.createNewUser("test", "", "");
        } catch(UserExistsException e) {
            ambiguous = true;
        }

        assertTrue("createNewUser() should throw an UserExistsException.", ambiguous);

    }


}
