package com.critical_web.blog.services.impl;

import com.critical_web.blog.services.SecurityService;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: rezz
 * Date: 25/12/13
 * Time: 1:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class DefaultSecurityServiceTest {

    SecurityService securityService = new DefaultSecurityService();

    @Test
    public void testEncrypt() {
        String expected = "62b25b630cd016a0f56c8097f84bf959a58bf9725cf38742ce92cc77dfea3e21468f3e7b59ebf33cc8b15fdf2f4b2b85d8183614bd442af4103bae7d57f602ec";
        String salt = "dMpxKY_d4zdyeSUD917-4Q";

        System.out.println(securityService);

        assertEquals("Unexpected password", expected, securityService.encryptPassword("admin", salt));

    }

}
