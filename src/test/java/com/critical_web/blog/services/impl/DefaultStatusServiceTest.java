package com.critical_web.blog.services.impl;

 /*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
 
 /*
 * Author: flevesque
 *
 */


import com.critical_web.blog.daos.hibernate.HibernatePropertyDao;
import com.critical_web.blog.entities.Property;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class DefaultStatusServiceTest
{

	DefaultStatusService statusService;

	@Mock
	HibernatePropertyDao propertyDao;

	@Before
	public void setUp()
	{
		initMocks(this);
		statusService = new DefaultStatusService();
		statusService.setPropertyDao(propertyDao);
	}

	@Test
	public void testIsInitialized()
	{

		final Property prop = new Property();
		prop.setId("isInitialized");
		prop.setValue("true");

		when(propertyDao.getProperty("isInitialized")).thenReturn(prop);

		assertTrue("isSystemInitialized() should return [true], returned [" + statusService.isSystemInitialized() + "]" +
				".", statusService.isSystemInitialized());

		prop.setValue("false");

		assertFalse("isSystemInitialized() should return [false], returned [" + statusService.isSystemInitialized() +
				"].", statusService.isSystemInitialized());

	}

}
