package com.critical_web.blog.form.validation;

 /*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
 
 /*
 * Author: flevesque
 *
 */


import com.critical_web.blog.form.AdminUser;
import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;
import org.hibernate.validator.resourceloading.PlatformResourceBundleLocator;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.*;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class MustMatchValidatorTest
{

	private static Validator validator;

	@BeforeClass
	public static void setUp() {
		Configuration<?> config = Validation.byDefaultProvider().configure();
		config.messageInterpolator(new ResourceBundleMessageInterpolator(new PlatformResourceBundleLocator("ValidationMessages")));

		ValidatorFactory factory = config.buildValidatorFactory();
		validator = factory.getValidator();
	}

	@Test
	public void testValidator() {

		final AdminUser adminUser = new AdminUser();
		adminUser.setUsername("test");
		adminUser.setPassword("match");
		adminUser.setRepassword("non-match");
		adminUser.setEmail("test");

		Set<ConstraintViolation<AdminUser>> violations = validator.validate(adminUser);

		assertEquals(2, violations.size());

		adminUser.setRepassword("match");

		violations = validator.validate(adminUser);

		assertEquals(1, violations.size());

		adminUser.setEmail("test@test.com");

		violations = validator.validate(adminUser);

		assertEquals(0, violations.size());

	}

}
